Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :samples, only: [:index, :create]
    end
  end

  resources :samples, only: [:index]
  root to: 'samples#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
