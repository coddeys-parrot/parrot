class FutureValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value > Time.now
      record.errors[attribute] << (options[:message] || "can't be in the future!")
    end
  end
end
