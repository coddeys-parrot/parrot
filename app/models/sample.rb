class Sample < ApplicationRecord
  validates_uniqueness_of :capture_time
  validates :capture_time, future: true

  scope :sensor, -> (sensor_id) { where sensor_id: sensor_id }
  scope :time_between, lambda {|start_time, end_time| where("capture_time >= ? AND capture_time <= ?", start_time, end_time )}
  scope :from_time, lambda {|start_time| where("capture_time >= ?", start_time)}
  scope :before_time, lambda {|end_time| where("capture_time <= ?", end_time )}
end
