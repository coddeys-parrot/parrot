module Api
  module V1
    class SamplesController < ApplicationController
      def index
        if params.has_key?(:sensor_id)
          @samples = Sample.sensor(params[:sensor_id])
          @samples = @samples.from_time(params[:start_time]) if params[:start_time].present?
          @samples = @samples.before_time(params[:end_time]) if params[:end_time].present?
          render json: @samples
        else
          render :json => { :errors => 'Params sensor_id is mandatory' }
        end
      end

      def create
        buffer = params[:buffer]
        es = Decoder.new.decode(buffer)
        @samples = es[:entres].each do |e|
          e[:capture_time] = Time.at(e[:capture_time]).to_datetime
          sample = Sample.new(e)
          if sample.save
          else
            ActiveRecord::Base.logger.info "Sample already present in the database #{sample.errors.messages.to_s}"
          end
        end

        render json: @samples ,status: :created
      end
    end
  end
end
