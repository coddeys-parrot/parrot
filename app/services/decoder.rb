class Decoder
  def perform(file)
    hash = JSON.parse(file)
    decode(hash['buffer'])
  end

  def decode(string)
    string = Base64.decode64(string)
    {}.tap do |sample|
      sample[:file_version], sample[:nb_entres] = string.unpack('CS>')
      sample[:entres] = []
      sample[:nb_entres].times do |n|
        a = string[(3 + 12 * n)..(14 + 12 * n)].unpack('L>S>S>S>S>')
        entry = { capture_time: a[0], sensor_id: a[1], light: a[2],
                  soil_moisture: a[3], air_temperature: a[4] }
        sample[:entres] << entry
      end
    end
  end
end
