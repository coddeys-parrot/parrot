json.array!(@samples) do |sample|
  json.extract! sample, :id, :capture_time, :sensor_id, :light, :soil_moisture, :air_temperature
  json.url sample_url(sample, format: :json)
end
