require 'rails_helper'

RSpec.describe 'Samples', type: :request do
  describe 'GET /samples' do
    it 'works!' do
      get samples_path
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET api/v1/samples' do
    context 'empty' do
      before(:example) { get api_v1_samples_path(sensor_id: 1) }
      it { expect(response).to have_http_status(200) }
      it { expect(response.body).to eq '[]' }
    end

    context 'has one sample' do
      before(:example) do
        Sample.create(capture_time: Time.new("2016-01-01T02:00:00.000Z"), sensor_id: 1,
                       light: 2, soil_moisture: 10, air_temperature: 1 )
        get api_v1_samples_path(sensor_id: 1)
      end
      it { expect(response).to have_http_status(200) }
      it { expect(response.body).to eq Sample.all.to_json }
    end

    context 'search by sensor id (mandatory)' do
      before(:example) do
        Sample.create(capture_time: Time.new("2016-01-02T02:12:00.000Z"), sensor_id: 3,
                       light: 2, soil_moisture: 10, air_temperature: 1 )
        Sample.create(capture_time: Time.new("2016-01-03T02:00:00.000Z"), sensor_id: 2,
                       light: 2, soil_moisture: 10, air_temperature: 1 )
        get api_v1_samples_path(sensor_id: 3)
      end
      it { expect(response).to have_http_status(200) }
      it { expect(response.body).to eq Sample.where(sensor_id: 3).to_json }
    end
  end

  describe 'POST api/v1/samples' do
    context 'create one sample' do
      let(:result) { "[{\"capture_time\":\"2016-04-04T13:02:39.000-03:00\",\"sensor_id\":37,\"light\":44223,\"soil_moisture\":374,\"air_temperature\":774}]" }
      before(:example) do
        json = '{ "buffer": "AAABVwKQHwAlrL8BdgMG\n" }'
        headers = { "CONTENT_TYPE" => "application/json" }
        post '/api/v1/samples', json, headers
      end
      it { expect(response.body).to eq result }      
    end

    context 'create one sample' do
      before(:example) do
        json = '{ "buffer": "AAADVwKQHwAlrL8BdgMGVwKMmwAloxgBeAMHVwKJFwAldmMBeQMH\n" }'
        headers = { "CONTENT_TYPE" => "application/json" }
        post '/api/v1/samples', json, headers
      end
      it { expect(Sample.all.count).to eq 3 }
    end
  end
end
