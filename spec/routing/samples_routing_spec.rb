require 'rails_helper'

RSpec.describe SamplesController, type: :routing do
  describe 'routing' do
    context 'app' do
      it 'routes to #index' do
        expect(get: '/samples').to route_to('samples#index')
      end
    end

    context 'api/v1' do
      it 'routes to #index' do
        expect(get: 'api/v1/samples').to route_to('api/v1/samples#index')
      end
    end
  end
end
