require 'rails_helper'

RSpec.describe Decoder do
  let(:decoder) { described_class.new }

  describe '#initialize' do
    it { expect(decoder).to be_an_instance_of Decoder }
  end

  describe '#perform' do
    context 'first one' do
      let(:json) { File.read(File.join('spec/fixtures', '/example_input_1.json')) }
      let(:result) do
        { file_version: 0, nb_entres: 1, entres: [
          { capture_time: 1_459_785_759,
            sensor_id: 37,
            light: 44_223,
            soil_moisture: 374,
            air_temperature: 774 }] }
      end

      it { expect(decoder.perform(json)).to eq result }
    end
    context 'second one' do
      let(:json) { File.read(File.join('spec/fixtures', '/example_input_2.json')) }
      let(:result) do
        { file_version: 0, nb_entres: 3, entres: [{:capture_time=>1459785759, :sensor_id=>37, :light=>44223, :soil_moisture=>374, :air_temperature=>774}, {:capture_time=>1459784859, :sensor_id=>37, :light=>41752, :soil_moisture=>376, :air_temperature=>775}, {:capture_time=>1459783959, :sensor_id=>37, :light=>30307, :soil_moisture=>377, :air_temperature=>775}] }
      end

      it { expect(decoder.perform(json)).to eq result }
    end
    context 'full one' do
      let(:json) { File.read(File.join('spec/fixtures', '/example_input.json')) }
      let(:result) do
        {:capture_time=>1459087361, :sensor_id=>37, :light=>15226,
         :soil_moisture=>377, :air_temperature=>717}
      end
      it { expect(decoder.perform(json)[:file_version]).to eq 0 }
      it { expect(decoder.perform(json)[:nb_entres]).to eq 7781 }
      it { expect(decoder.perform(json)[:entres][776]).to eq result }
    end
  end
end
