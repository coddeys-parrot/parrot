require 'rails_helper'

RSpec.describe 'samples/new', type: :view do
  before(:each) do
    assign(:sample, Sample.new(
                      sensor_id: 1,
                      light: 1,
                      soil_moisture: 1,
                      air_temperature: 1
    ))
  end

  it 'renders new sample form' do
    render

    assert_select 'form[action=?][method=?]', samples_path, 'post' do
      assert_select 'input#sample_sensor_id[name=?]', 'sample[sensor_id]'

      assert_select 'input#sample_light[name=?]', 'sample[light]'

      assert_select 'input#sample_soil_moisture[name=?]', 'sample[soil_moisture]'

      assert_select 'input#sample_air_temperature[name=?]', 'sample[air_temperature]'
    end
  end
end
