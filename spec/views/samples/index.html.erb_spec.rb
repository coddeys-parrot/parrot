require 'rails_helper'

RSpec.describe 'samples/index', type: :view do
  before(:each) do
    assign(:samples, [
             Sample.create!(
               capture_time: Time.zone.now,
               sensor_id: 2,
               light: 3,
               soil_moisture: 4,
               air_temperature: 5
             ),
             Sample.create!(
               capture_time: Time.zone.now - 1.minute,
               sensor_id: 2,
               light: 3,
               soil_moisture: 4,
               air_temperature: 5
             )
           ])
  end

  it 'renders a list of samples' do
    render
    assert_select 'tr>td', text: 2.to_s, count: 2
    assert_select 'tr>td', text: 3.to_s, count: 2
    assert_select 'tr>td', text: 4.to_s, count: 2
    assert_select 'tr>td', text: 5.to_s, count: 2
  end
end
