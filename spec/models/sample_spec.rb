require 'rails_helper'

RSpec.describe Sample, type: :model do
  describe 'validate capture_date' do
    context 'uniqueness' do
      it 'valid' do
        expect(Sample.create(capture_time: Time.now)).to be_valid
      end
      it 'not_valid' do
        time = Time.now
        Sample.create(capture_time: time)
        expect(Sample.create(capture_time: time)).not_to be_valid
      end
    end
  end
  describe 'validate capture_date' do
    context 'future time' do
      it 'valid' do
        expect(Sample.create(capture_time: (Time.now - 1.day))).to be_valid
      end
      it 'not_valid' do
        expect(Sample.create(capture_time: (Time.now + 1.day))).not_to be_valid
      end
    end
  end
end
